export const WorkoutActionTypes = {
  SELECT_WORKOUT: "SELECT_WORKOUT",
};

export const selectWorkout = (selectedWorkout) => {
  return ({
    type: WorkoutActionTypes.SELECT_WORKOUT,
    payload: selectedWorkout
  })
};