import {WorkoutActionTypes} from "../actions/WorkoutActions";

const { SELECT_WORKOUT } = WorkoutActionTypes;

export const workoutReducer = (state = [], action) => {
  switch(action.type) {
    case SELECT_WORKOUT: {
      return {
        ...state,
        selectedWorkout: action.payload
      };
    }
    default: {
      return state;
    }
  }
};