import React from 'react';
import Routes from "./src/screens/Routes";
import {Provider} from "react-redux";
import {createStore} from "redux";
import rootReducer from "./src/redux/reducers/index";

const store = createStore(rootReducer);

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Routes/>
      </Provider>
    );
  }
}
