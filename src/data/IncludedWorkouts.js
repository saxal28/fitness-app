export const IncludedWorkouts = {

  '531': {
    id: "531",
    label: "5/3/1",
    routine: [
      { exercise: 'Bench Press', isMainLift: true, isAccessoryLift: false }
    ]
  },
  'GreySkullPPL': {
    id: 'grayskullppl',
    label: "Grayskull's PPL",
    routine: [
      { exercise: 'Bench Press', isMainLift: true, isAccessoryLift: false }
    ]
  },
  'StartingStrength': {
    id: 'startingstrength',
    label: "Starting Strength",
    routine: [
      { exercise: 'Bench Press', isMainLift: true, isAccessoryLift: false }
    ]
  },
  'Stronglifts': {
    id: "stronglifts",
    label: "Stronglifts 5x5",
    routine: [
      { exercise: 'Bench Press', isMainLift: true, isAccessoryLift: false }
    ]
  }

};

function getList() {
  const arr = [];
  for (let workout in IncludedWorkouts) {
    arr.push(IncludedWorkouts[workout]);
  }
  console.log(arr);
  return arr;
}

export const IncludedWorkoutsList = getList();

